#!/bin/bash

apply() {

PUPPET_PATH=$(pwd)/puppet
cd $PUPPET_PATH
sudo puppet apply --modulepath $PUPPET_PATH/modules --hiera_config $PUPPET_PATH/hiera.yaml $PUPPET_PATH/manifests/site.pp --debug

}

case "$1" in

   apply)
    echo -e "RUNNING PUPPET WITH FACTER_role=$2"
           export FACTER_role="$2"
           apply 

    ;;
   *)
  N="./run.sh apply default OR custom YAML DEFINITION."

  echo "YOU MUST READ Readme.md FILE FIRST -- TO RUN PUPPET APPLY ON TARGET NODE DO $N" >&2

  exit 1
  ;;

esac

exit 0
