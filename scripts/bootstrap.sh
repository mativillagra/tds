#!/usr/bin/env bash

rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm

yum install puppet-3.8.2 rsync -y

gem install librarian-puppet -v 1.5.0

adduser provision -d /home/provision -s /bin/bash

touch /etc/sudoers.d/provision

cat >> /etc/sudoers.d/provision <<EOL
%provision ALL=NOPASSWD:/usr/bin/puppet
Defaults env_keep += FACTER_*
EOL

su provision <<'EOF'

mkdir -p /home/provision/.ssh

chmod 700 /home/provision/.ssh

touch /home/provision/.ssh/authorized_keys

cat >> /home/provision/.ssh/authorized_keys <<EOL

EOL

chmod 600 /home/provision/.ssh/authorized_keys
EOF