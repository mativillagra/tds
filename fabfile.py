from fabric.api import *
from fabric.contrib.project import rsync_project
import ConfigParser

env.hosts = [
    '10.0.15.24',
]


@task
def bootstrap(remote_user='root',bootstrap_file='./scripts/bootstrap.sh'):
    env.user=remote_user
    put("{}".format(bootstrap_file), "/root/bootstrap.sh", mode=0755, use_sudo=True)
    sudo("/root/bootstrap.sh")


@task
def provision(remote_user='provision',role_string='null',home_user='/home/provision'):
    env.user=remote_user
    env.use_ssh_config = True
    env.print_only = False
    
    rsync_project("{}".format(home_user), "./puppet",delete= True, ssh_opts='-o StrictHostKeyChecking=no')
    environment_facts={}
    environment_facts['FACTER_role'] = role_string    
    
    modulepath="{}/puppet/modules".format(home_user)
    manifests="{}/puppet/manifests".format(home_user)
    hiera_config="{}/puppet/hiera.yaml".format(home_user)
    pp="{}/puppet/manifests/site.pp".format(home_user)
    with cd('puppet'), settings(warn_only=True),shell_env(**environment_facts):
     result = run('sudo puppet apply --modulepath {} --hiera_config {} {} --debug'.format(modulepath,hiera_config,pp))
