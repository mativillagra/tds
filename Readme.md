#TDS Interview Puppet Evaluation


This doc is used to guide on how to used this repo.

Two ways to provision a node using this puppet code. 

# Installation 1

Pre Requirements on target node.

- Puppet agent must be pre-installed on the target node.

- Add this line to the user you will used to run this puppet code. 

	echo "Defaults env_keep += FACTER_*" >> /etc/sudoers.d/your_user_account

1. Unzip the code 

2. RUN ./run.sh 



# Installation 2

Pre Requirements on your host OS.

- Vagrant 1.8.1

- Fabric 1.8.2

- Virtual Box 5.0.20 or above.


1. Unzip the code 

2. Add your /home/your_user_account/.ssh/id_rsa key into the bootstrap.sh script on line 27 save & quit

3. Initiate the  Centos 6 Virtual Box instance on your Virtual Box Software by 

	vagrant up tds

4. Run the puppet code with fabric python framework by

	fab provision:role_string='default' OR fab provision:role_string='custom'
