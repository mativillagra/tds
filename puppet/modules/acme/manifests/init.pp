class acme 

{

$acme_config_hash     = hiera_hash('acme::config_hash', undef, 'hiera-file') 
$acme_group           = $acme_config_hash["ACME_GROUP"]["value"]
$acme_group_gid       = $acme_config_hash["ACME_GROUP_GID"]["value"]
$acme_user            = $acme_config_hash["ACME_USER"]["value"]
$acme_uid             = $acme_config_hash["ACME_UID"]["value"]
$acme_shell           = $acme_config_hash["ACME_SHELL"]["value"]
$install_dir          = hiera('install_dir')
$package_list         = hiera("package_list")
$enbaled_settings     = hiera("enbaled_settings")


if $enbaled_settings == true

{
	
 group { "${acme_group}":
  ensure => 'present',
  gid    => "${acme_group_gid}",
 } ->

user { "${acme_user}":
  ensure           => 'present',
  gid              => "${acme_group_gid}",
  shell            => "${acme_shell}",
  uid              => "${acme_uid}",
 } ->

 file { "${$install_dir}":
  ensure   => 'directory',
  owner    => "${acme_user}",
  group    => "${acme_group}",
  mode     => '0600',
} ->

 file { "$rubysitedir/facter/free_space.rb":
  ensure  => file,
  content => template('acme/free_space.rb.erb'),
  mode    => '0644',
  notify  => File["${$install_dir}/acme_app.ini"],
 } ->

 file { "${$install_dir}/acme_app.ini":
  ensure    => file,
  content   => template('acme/acme_app.ini.erb'),
  mode      => '0644',
  subscribe => File["$rubysitedir/facter/free_space.rb"],
 }

 exec { 'log':
   command     => "logger ACME APP WAS INSTALLED",
   onlyif      => "test ! -d ${$install_dir}",
   user        => "root",
 }

 include acme::packages

}

else

{

 user { "${acme_user}":
  ensure           => 'absent',
  gid              => "${acme_group_gid}",
  shell            => "${acme_shell}",
  uid              => "${acme_uid}",
 } ->

 group { "${acme_group}":
  ensure => 'absent',
  gid    => "${acme_group_gid}",
 } ->

 file { 'delete':
   ensure  => absent,
   path    => "${$install_dir}",
   recurse => true,
   purge   => true,
   force   => true,
 }

}

}